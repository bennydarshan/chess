#define BOARD 8
#define TILE 64
#define PIECE 112
#define BUFF 64
#define MINMAX_LIMIT 6

#include <limits.h>
#include <pthread.h>

#include <SDL2/SDL.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

enum SLD_TYPE{PAWN= 1, ROOK = 2, KNIGHT = 3, BISHOP = 4, KING = 6, QUEEN = 5}; 

struct coord{
	int y;
	int x;
};

struct move{
	struct coord from;
	struct coord to;
};

struct ghost{
	unsigned char piece;
	int y;
	int x;
};

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
int running;

void draw_map(SDL_Renderer *ren) {
	SDL_Rect iter = {.x = 0, .y = 0, .h = TILE, .w = TILE};
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	for(int i=0;i<BOARD;i++) for(int j=0;j<BOARD;j+=2) {
		iter.y = i * TILE;
		const int offset = (i%2) == 1;
		iter.x = (j+offset) * TILE;
		SDL_RenderFillRect(ren, &iter);
	}


}

int is_black(unsigned char self)
{
	return (self >> 7);
}
unsigned char get_type(unsigned char self)
{
	unsigned char tmp = self << 1;
	return (tmp >> 1);
}


void draw_piece(SDL_Renderer *ren, SDL_Texture *tex, unsigned char map[BOARD][BOARD], struct ghost *g)
{
	SDL_Rect dest = {.x = 0, .y = 0, .h = TILE, .w = TILE};
	SDL_Rect src = {.x = 0, .y = 0, .h = PIECE, .w = PIECE};
	for(int i=0;i<BOARD;i++) for(int j=0;j<BOARD;j++) {
		if(map[i][j] == 0) continue;
		
		unsigned char iter = get_type(map[i][j]);
		if(is_black(map[i][j]))
			src.y = PIECE;
		else src.y = 0;
		src.x = ( 7 - iter-1) * PIECE;
		dest.x = j * TILE;
		dest.y = i * TILE;
		SDL_RenderCopy(ren, tex, &src, &dest);
	}
	if(is_black(g->piece))
		src.y = PIECE;
	else src.y = 0;
	src.x = ( 7 - get_type(g->piece)-1) * PIECE;
	dest.x = g->x * TILE;
	dest.y = g->y * TILE;
	SDL_SetTextureAlphaMod(tex, 50);
	SDL_RenderCopy(ren, tex, &src, &dest);
	SDL_SetTextureAlphaMod(tex, 255);

}

void set_coord(struct coord *self, int x, int y) 
{
	self->x = x;
	self->y = y;
}

int inbound(struct coord *self)
{
	return ((self->x >= 0) && (self->y >= 0) && (self->x < BOARD) && (self->y < BOARD));
}

int check_empty(int cx, int cy, int offset, unsigned char map[BOARD][BOARD])
{
	int skip = offset > cx ? 1 : -1;
	for(int i = cx + skip; i != offset; i+= skip) {
		if(map[cy][i]) return 0;
	}
	return 1;
}

int options(int cx, int cy, unsigned char map[BOARD][BOARD], struct coord out[BUFF], unsigned char *castling)
{
	unsigned char type = get_type(map[cy][cx]);
	if(type == 0) return 0;

	struct coord offsets[10];
	struct coord *ptr = offsets;
	int offsets_size = 0;
	int distance_limiter = BOARD;


	if(type == PAWN) {
		int black = is_black(map[cy][cx]);
		struct coord tmp;
		int at_start = black ? cy == 1 : cy == 6;
		if(at_start) {
			tmp.x = 0;
			tmp.y = (black ?  2 :  -2);
			if(!map[cy + tmp.y][cx + tmp.x])
				offsets[offsets_size++] = tmp;
		}
		tmp.x = 0;
		tmp.y = (black ? 1 : -1);
		if(!map[cy + tmp.y][cx + tmp.x])
			offsets[offsets_size++] = tmp;
		else
			offsets_size = 0;
		
		tmp.x = 1;
		if((map[cy + tmp.y][cx + tmp.x]) && (black != is_black(map[cy + tmp.y][cx + tmp.x]))) offsets[offsets_size++] = tmp;
		tmp.x = -1;
		if((map[cy + tmp.y][cx + tmp.x]) && (black != is_black(map[cy + tmp.y][cx + tmp.x]))) offsets[offsets_size++] = tmp;

		distance_limiter = 1;


	}
	if((type == ROOK) || (type == QUEEN) || (type == KING)) {
		set_coord(ptr++, 1, 0);
		set_coord(ptr++, 0, 1);
		set_coord(ptr++, -1, 0);
		set_coord(ptr++, 0, -1);
		offsets_size = 4;
		distance_limiter = BOARD;
	}
	if((type == BISHOP) || (type == QUEEN) || (type == KING)) {
		set_coord(ptr++, 1, 1);
		set_coord(ptr++, 1, -1);
		set_coord(ptr++, -1, 1);
		set_coord(ptr++, -1, -1);
		offsets_size = 4;
		distance_limiter = BOARD;
	}
	if(type == KNIGHT) {
		set_coord(ptr++, 2, 1);
		set_coord(ptr++, 1, 2);
		set_coord(ptr++, -2, 1);
		set_coord(ptr++, -1, 2);
		set_coord(ptr++,2, -1);
		set_coord(ptr++,1, -2);
		set_coord(ptr++, -2, -1);
		set_coord(ptr++, -1, -2);
	}
	if((type == QUEEN) || (type == KING) || (type == KNIGHT))
		offsets_size = 8;

	if((type == KING)||(type == KNIGHT)) distance_limiter = 1;
	if(type == KING) {
		int black = is_black(map[cy][cx]);
		unsigned tmp_castling = 0;
		if(black) tmp_castling = *castling;
		else tmp_castling = ((*castling) >> 2);
		if(((tmp_castling & 1) == 1) && (check_empty(cx, cy, BOARD - 1, map))) set_coord(ptr++, BOARD -1 - cx, 0);
		if(((tmp_castling & 2) == 2) && (check_empty(cx, cy, 0, map))) set_coord(ptr++, 0 - cx, 0);
		offsets_size+=2;

	}


	int options_size = 1;
	set_coord(out, cx, cy);
	for(int i=0;i<offsets_size;i++) for(int j=1;j<=distance_limiter;j++) {
		struct coord tmp = {cy + offsets[i].y*j, cx + offsets[i].x*j};
		if((tmp.x < 0) || (tmp.x >= BOARD) || (tmp.y < 0) || (tmp.y >= BOARD)) break;
		unsigned char victim;
		if(victim = map[tmp.y][tmp.x]) {
			if((is_black(victim) != is_black(map[cy][cx])) || ((get_type(victim) == ROOK) && (get_type(map[cy][cx]) == KING))) out[options_size++] = tmp;
			break;
		}
		out[options_size++] = tmp;
	}


	return options_size;



	

}

void marker_at(SDL_Renderer *ren, int x, int y, int isblack)
{
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 127);
	SDL_Rect glow = {
		.x = x * TILE,
		.y = y * TILE,
		.h = TILE,
		.w = TILE};
	SDL_RenderFillRect(ren, &glow);
}


struct move handle_click(unsigned char map[BOARD][BOARD], int *turn, struct coord out[BUFF],int *out_size, unsigned char *castling) 
{
	struct move ret = {{-1, -1}, {-1, -1}};
	int x,y;
	SDL_GetMouseState(&x, &y);
	x/=TILE;
	y/=TILE;
	struct coord dest = {y,x};
	for(int i=1; i<*out_size; i++) {
		if((out[i].x == dest.x) &&(out[i].y == dest.y)) {
			ret.from.y = out[0].y;
			ret.from.x = out[0].x;
			ret.to.y = dest.y;
			ret.to.x = dest.x;
			*out_size = 0;
			return ret;
		}
	}

	if(map[y][x] && (is_black(map[y][x]) == *turn) && (!*out_size || (is_black(map[out[0].y][out[0].x]) == is_black(map[y][x])))) {
		*out_size = options(x, y, map, out, castling);
		return ret;
	}
	return ret;
}

void set_castling(unsigned char *self, struct move *m, unsigned char piece)
{
	if(get_type(piece) == KING) {
		const unsigned char opp = is_black(piece) ? ~2 : ~12;
		*self &= opp;
		return;
	}

	unsigned char opp = 0;
	opp = is_black(piece) && (m->from.x == BOARD - 1) && (m->from.y == 0);
	opp |= (is_black(piece) && (m->from.x == 0) && (m->from.y == 0)) << 1;
	opp |= (!is_black(piece) && (m->from.x == BOARD - 1) && (m->from.y == BOARD - 1)) << 2;
	opp |= (!is_black(piece) && (m->from.x == 0) && (m->from.y == BOARD - 1)) << 3;

	*self &= ~opp;

}

void check_mate(unsigned char self, int *turn)
{
	if(get_type(self) == KING)
		*turn = is_black(self)? -1: -2;
	else
		*turn = ((*turn) + 1) % 2;

}

void set_ghost(struct ghost *self,unsigned char piece, int x, int y)
{
	self->piece = piece;
	self->x = x;
	self->y = y;
}

int handle_move(struct move *self, unsigned char map[BOARD][BOARD], int *turn, struct ghost *prev, unsigned char *castling)
{
	static unsigned int move_count = 0;
	if(self->from.x == -1 ) return 0;
	pthread_mutex_lock(&lock);
	unsigned piece = map[self->from.y][self->from.x];
	map[self->from.y][self->from.x] = 0;
	set_ghost(prev, piece, self->from.x, self->from.y);
	check_mate(map[self->to.y][self->to.x], turn);
	set_castling(castling, self, piece);
	if ((map[self->to.y][self->to.x]) && (is_black(piece) == is_black(map[self->to.y][self->to.x]))) {
			if(self->to.x == 0) {
				map[self->to.y][self->to.x + 2] = piece;
				map[self->to.y][self->to.x + 3] = map[self->to.y][self->to.x];
				map[self->to.y][self->to.x] = 0;
			}else if(self->to.x == (BOARD - 1)) {
				map[self->to.y][self->to.x - 1] = piece;
				map[self->to.y][self->to.x - 2] = map[self->to.y][self->to.x];
				map[self->to.y][self->to.x] = 0;
			}
	} else
		map[self->to.y][self->to.x] = piece;
	if(get_type(piece) == PAWN)
		if((self->to.y == 0) || (self->to.y == BOARD - 1) )
			map[self->to.y][self->to.x] |= QUEEN;
	printf("%3d: (%d, %d) -> (%d, %d)\n", move_count++, self->from.x, self->from.y, self->to.x, self->to.y);
	self->from.x = -1;
	pthread_mutex_unlock(&lock);
	return 1;

}
void draw_options(SDL_Renderer *ren, struct coord *options, int size)
{
	for(int i=1;i<size;i++)
		marker_at(ren, options[i].x, options[i].y, 0);

}

SDL_Texture *load_tex(SDL_Renderer *ren, const char *path)
{
	int width, height;
	int *image_pixels = (int *)stbi_load(path, &width, &height, NULL, 4);

	SDL_Surface* image_surface = SDL_CreateRGBSurfaceFrom(image_pixels, (int) width, (int) height, 32, (int) width * 4, 0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000);

	SDL_Texture *ret = SDL_CreateTextureFromSurface(ren, image_surface);
	return ret;

}

// AI BITS

int uristic(unsigned char map[BOARD][BOARD])
{
	int score = 0;
	for(int i=0;i<BOARD;i++) for(int j=0;j<BOARD;j++) {
		int type = get_type(map[i][j]);
		if(type == KING) type = 3000;
		if(!is_black(map[i][j])) type *= -1; 
		score += type;

	}
	return score;
}

void move_set(struct move *self, int fx, int fy, int tx, int ty)
{
	self->from.x = fx;
	self->from.y = fy;
	self->to.x = tx;
	self->to.y = ty;
}

int minmax(unsigned char map[BOARD][BOARD], int alpha, int beta, int maxplayer, int iter, struct move *out, unsigned char castling)
{
	if(!iter) 
		return maxplayer? uristic(map) : -1 * uristic(map);
	struct coord buff[64];
	int buff_logic_size = 0;
	for(int i=0;i<BOARD;i++) for(int j=0;j<BOARD;j++) {
		unsigned char cur = map[i][j];
		if(!is_black(cur) == maxplayer) continue;
		buff_logic_size = options(j, i, map, buff, &castling);
		for(int k=1;k<buff_logic_size;k++) {
			//moving here to save memory
			unsigned char next_castling = castling;
			struct move m = { j, i, buff[k].x, buff[k].y};
			set_castling(&next_castling, &m, map[i][j]);
			map[i][j] = 0;
			unsigned destory = map[buff[k].y][buff[k].x];
			map[buff[k].y][buff[k].x] = cur;
			
			if( ((buff[k].y == 0) || (buff[k].y == BOARD - 1)) && (get_type(cur) == PAWN)) map[buff[k].y][buff[k].x] |= QUEEN;
			int cur_value = -1 * minmax(map, -1 * beta, -1 * alpha, !maxplayer, iter - 1, NULL, next_castling);
			if(( cur_value) > alpha) {
				alpha =  cur_value;
				if(out) move_set(out, j, i, buff[k].x, buff[k].y);
				if(alpha >= beta) {
					map[buff[k].y][buff[k].x] = destory;
					map[i][j] = cur;
				       	return alpha;
					//break;
				}

			}
			map[buff[k].y][buff[k].x] = destory;
		}
		map[i][j] = cur;

	}
	return alpha;
	
}

void initiate_minmax(unsigned char map[BOARD][BOARD], struct move *out, unsigned char *castling)
{
	while(running) {
		struct move out_tmp;
		pthread_mutex_lock(&lock);
		pthread_cond_wait(&cond, &lock);
		unsigned char map_tmp[BOARD][BOARD];
		for(int i=0;i<BOARD;i++) for(int j=0;j<BOARD;j++) {
			map_tmp[i][j] = map[i][j];
		}
		int alpha = INT_MIN + 1, beta = INT_MAX;
		int score = minmax(map_tmp, alpha, beta, 1, MINMAX_LIMIT, &out_tmp, *castling); 
		*out = out_tmp;
		pthread_mutex_unlock(&lock);
	}
}

struct args{
	unsigned char (*map)[BOARD][BOARD];
	struct move *out;
	unsigned char *castling;
};

void *minmax_thread(void *args)
{
	struct args *args_t = (struct args *)args;
	initiate_minmax(*(args_t->map),args_t->out, args_t->castling);
	return NULL;
}

//END AI


void update_wintitle(SDL_Window *win, int *turn)
{
	switch(*turn) {
		case 0: SDL_SetWindowTitle(win, "White Turn");
			break;
		case 1: SDL_SetWindowTitle(win, "Black Turn");
			break;
		case -1: SDL_SetWindowTitle(win, "White Wins");
			 break;
		case -2: SDL_SetWindowTitle(win, "Black Wins");
			 break;
	}
}

int main()
{
	running = 1;
	unsigned char opp = 1;
	opp = opp << 7;
	pthread_t ai_thread;
	
	unsigned char map[BOARD][BOARD] = {
		{ROOK | opp, KNIGHT | opp, BISHOP | opp, QUEEN | opp, KING | opp, BISHOP | opp, KNIGHT | opp, ROOK | opp},
		{PAWN | opp, PAWN | opp, PAWN | opp, PAWN | opp, PAWN | opp, PAWN | opp, PAWN | opp, PAWN | opp},
		{0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0},
		{PAWN, PAWN, PAWN, PAWN, PAWN, PAWN, PAWN, PAWN},
		{ROOK, KNIGHT, BISHOP, QUEEN, KING, BISHOP, KNIGHT, ROOK}

	};

	SDL_Window *win;
	SDL_Renderer *ren;
	SDL_Texture *tex;
	SDL_Event e;

	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
	const int board_size = BOARD * TILE;
	win = SDL_CreateWindow("Chess", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, board_size, board_size, SDL_WINDOW_OPENGL);
	ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);
	if(!ren) {
		fprintf(stderr, "Failed to create renderer\n");
		return 1;
	}
	tex = load_tex(ren, "res.png");
	SDL_SetRenderDrawBlendMode(ren, SDL_BLENDMODE_BLEND);
	struct coord buff[BUFF];
	struct move cur_move = {{-1, -1}, {-1, -1}};
	struct ghost last_ghost = {0, -1, -1};
	unsigned char castling = 15;
	int buff_size = 0;
	int turn = 0;
	struct args ai_args;
	ai_args.map = &map;
	ai_args.out = &cur_move;
	ai_args.castling = &castling;
	pthread_create(&ai_thread, NULL, minmax_thread, (void *) &ai_args);
	while(running)
	{
		while(SDL_PollEvent(&e)) {
			switch(e.type) {
				case SDL_QUIT:
					running = 0;
					break;
				case SDL_MOUSEBUTTONUP:
					cur_move = handle_click(map, &turn, buff, &buff_size, &castling);
					break;
			}
		}
		if(handle_move(&cur_move, map, &turn, &last_ghost, &castling)) {
			update_wintitle(win, &turn);
			if(turn == 1) pthread_cond_signal(&cond);
		}
		SDL_SetRenderDrawColor(ren, 97, 26, 9, 255);
		SDL_RenderClear(ren);
		draw_map(ren);
		draw_piece(ren, tex, map, &last_ghost);
		draw_options(ren, buff, buff_size);
		SDL_RenderPresent(ren);
		SDL_Delay(1000/60);

	}

	SDL_DestroyTexture(tex);
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);

	return 0;
}
